import argparse

import torch

from network_model.discriminator import Discriminator
from network_model.generator import Generator


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('generator_path', metavar='generator_path', type=str, help='generator model path')
    parser.add_argument('discriminator_path', metavar='output_path', type=str, help='discriminator model path')

    args = parser.parse_args()
    generator_path = getattr(args, 'generator_path')
    output_path = getattr(args, 'discriminator_path')

    model = Generator()
    model.load_state_dict(torch.load(generator_path))
    model.cuda()

    discriminator = Discriminator()
    discriminator.load_state_dict(torch.load(output_path))
    discriminator.cuda()

    device = torch.device("cuda:0")

    for i in range(10):
        with torch.no_grad():
            generator_noise = torch.randn(64, 100, 1, 1, device=device)
            fake = model(generator_noise)
            print(discriminator(fake.detach()).view(-1))


if __name__ == '__main__':
    main()
