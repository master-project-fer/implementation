import argparse
import os

import torch
from torchvision.utils import save_image

from network_model.generator import Generator


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('generator_path', metavar='generator_path', type=str, help='generator model path')
    parser.add_argument('--output', metavar='output_path', nargs='?', type=str, help='output path',
                        default='./generator_output')
    parser.add_argument('--count', metavar='count', nargs='?', type=int, help='number of samples',
                        default='10')

    args = parser.parse_args()
    generator_path = getattr(args, 'generator_path')
    output_path = getattr(args, 'output')
    if os.path.exists(output_path) and not os.path.isdir(output_path):
        raise Exception()
    elif not os.path.exists(output_path):
        os.makedirs(output_path)
    count = getattr(args, 'count')

    model = Generator()
    model.load_state_dict(torch.load(generator_path))
    model.cuda()

    device = torch.device("cuda:0")

    for i in range(count):
        with torch.no_grad():
            generator_noise = torch.randn(64, 100, 1, 1, device=device)
            fake = model(generator_noise)[0]
            save_image(fake, f'{output_path}/image-{i}.jpg')


if __name__ == '__main__':
    main()
